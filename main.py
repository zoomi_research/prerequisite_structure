import pandas as pd
import numpy as np
import math
import tensorflow as tf

def motif_map(motif):
    '''
    input: array numpy
    HE: 0
    LE: 1
    SB: 2
    SF:3
    Nan: 3 (another option: 4)
    '''
    index_array = ['HE','LE','SB','SF',np.nan]
    result = np.zeros([motif.shape[0], motif.shape[1]], dtype=np.int32)
    for iter_x in range(motif.shape[0]):
        for iter_y in range(motif.shape[1]):
            if motif[iter_x, iter_y] == 'RS': result[iter_x, iter_y] = 1
            else:result[iter_x,iter_y] = index_array.index(motif[iter_x,iter_y])
    np.place(result, result == 4, 1)
    return result

# Import data
motif = pd.read_csv('user_motif.csv') # segment output
motif = motif.iloc[:,1:] # remove first column index
embeddings = pd.read_csv('glove_embeddings.csv') # glove embeddings representing all text inputs
embeddings = embeddings.iloc[:,1:] # remove first column index
motif = motif.as_matrix()
motif = motif_map(motif)

# Global config variables
num_steps = 200 # number of learners per epoch
num_segments = motif.shape[1]
num_users = motif.shape[0] # number of users
num_classes = 4 # predicting HE/LE/SB/SF
state_size = embeddings.shape[0] # size of hidden state, k
learning_rate = 0.01 # for ADAMS
h_size = 10

# get training and validation sets
np.random.seed(0)
pct_val = 0.01
n_val = int(np.round(num_users * pct_val))
perm = np.random.permutation(num_users)
val_ids = perm[0:n_val]
motif_val = motif[val_ids,]
motif_train = motif[perm[n_val:],]
num_users_train = num_users - n_val
motif_val_pred = np.zeros([n_val,num_segments])

print 'setup done'

def get_batch(no_batches,motif_train):
    # partition raw data into batches and stack them vertically in a data matrix || y
    data_y = motif_train # .as_matrix()
    # data_y = motif_map(data_y)
    # further divide batch partitions into num_steps for truncated backprop
    N = motif_train.shape[0]

    for i in range(no_batches):
        randidx = np.random.permutation(N)[0]
        y_batch = data_y[randidx,]
        y_batch_full = (np.tile(y_batch[:,np.newaxis],[1,num_classes]) == \
                            np.tile(np.arange(num_classes)[np.newaxis,],[num_segments,1])).astype(float)
        # now turn into 1-hot
        yield (randidx,y_batch_full)

def gen_epochs(num_epochs, num_steps):
    for i in range(num_epochs):
        yield get_batch(num_steps, motif_train)





# new rnn code
# each batch has certain number of users
cock = tf.placeholder(dtype=tf.float32, shape=[state_size, num_segments])
sucker = tf.placeholder(dtype=tf.float32, shape=[num_segments, num_classes])
idx = tf.placeholder(dtype=tf.int32,shape=())
idx_val = tf.placeholder(dtype=tf.int32,shape=())

U = tf.get_variable('U', [h_size, state_size], initializer=tf.random_uniform_initializer(-0.1,0.1)) # input of glove
W = tf.get_variable('W', [h_size, h_size], initializer=tf.random_uniform_initializer(-0.1,0.1)) # weights
V = tf.get_variable('V', [num_classes, 2*h_size], initializer=tf.random_uniform_initializer(-0.1,0.1)) # weights for actions for gating
gall = tf.get_variable('gall', [h_size, num_users_train], initializer=tf.random_uniform_initializer(-0.1,0.1)) # prerequisite knowledge gap
p = tf.get_variable('p', [h_size, num_segments], initializer=tf.random_uniform_initializer(-0.1,0.1)) # required knowledge
R = tf.get_variable('R', [num_segments, num_segments], initializer=tf.random_uniform_initializer(0,0.1),constraint=lambda x: tf.clip_by_value(x, 0, np.infty)) # prerequisite matrix

def fp(idx,X,Y):
    # forward propagation
    T = X.shape[1]
    # do this shit recursively
    pf = []
    h = tf.zeros([h_size,1])
    hl_temp = tf.zeros([h_size,1])
    hl = []
    for tt in range(T):
        # first get the current latent state
        h = tf.matmul(W, h) + hl_temp
        wt = gall[:,idx][:,np.newaxis] - h
        # now calculate the gap using prerequite matrix R
        gt = p[:,tt][:,np.newaxis]
        for t in range(tt - 1, -1, -1):
            gt -= R[t, tt] * hl[tt-1]
        pf_temp = tf.matmul(V[:,0:h_size],gt) + tf.matmul(V[:,h_size:],wt) #tf.nn.softmax(tf.matmul(V[:,0:h_size],gt) + tf.matmul(V[:,h_size:],wt))
        pf.append(tf.reshape(pf_temp,[-1]))
        e = tf.nn.softmax(tf.matmul(V[0:2,0:h_size],gt) + tf.matmul(V[0:2,h_size:],wt))[0]
        #e = tf.cast(tf.argmax(Y[tt, :], 0), tf.float32)  # feed previous state
        hl_temp = e * tf.matmul(U, X[:,tt][:,np.newaxis])
        hl.append(hl_temp)

    return pf

#losses and train_step
pfs = fp(idx,cock,sucker)
losses = [tf.nn.softmax_cross_entropy_with_logits(labels=sucker[tt,], logits=pfs[tt]) for \
            tt in range(num_segments)]
total_loss = tf.reduce_mean(losses)
global_step = tf.Variable(0, name='global_step', trainable=False) # the number of batches seen by the graph
train_step = tf.train.AdagradOptimizer(learning_rate).minimize(total_loss, global_step=global_step)

# define a method to evaluate on the validation set
# the difficulty here is to feed in new g values - average g from training users
# and then evaluate on the test users
def eval(sucker,X):
    # forward propagation
    T = X.shape[1]
    # do this shit recursively
    pf = []
    pfm = []
    h = tf.zeros([h_size,1])
    hl_temp = tf.zeros([h_size,1]) # input from previous weights and h and glove
    hl = []
    for tt in range(T):
        # first get the current latent state
        h = tf.matmul(W, h) + hl_temp
        wt = tf.reduce_mean(gall,1)[:,np.newaxis] - h # the average over all g values over training users
        # now calculate the gap using prerequite matrix R
        gt = p[:,tt][:,np.newaxis]
        for t in range(tt - 1, -1, -1):
            gt -= R[t, tt] * hl[tt-1]
        pf_temp = tf.matmul(V[:,0:h_size],gt) + tf.matmul(V[:,h_size:],wt) #tf.nn.softmax(tf.matmul(V[:,0:h_size],gt) + tf.matmul(V[:,h_size:],wt))
        pf.append(tf.reshape(pf_temp,[-1]))
        pfm.append(np.argmax(tf.reshape(pf_temp,[-1]))) # indicator of class
        #e = tf.nn.softmax(tf.matmul(V[0:2,0:h_size],gt) + tf.matmul(V[0:2,h_size:],wt))[0]
        e = tf.cast(tf.argmax(sucker[tt,:],0),tf.float32) # feed previous state
        hl_temp = e * tf.matmul(U, X[:,tt][:,np.newaxis])
        hl.append(hl_temp)

    # print pfm
    return pf,pfm

#losses
pf_val,pred_eval = eval(sucker,cock)
losses_val = [tf.nn.softmax_cross_entropy_with_logits(labels=sucker[tt,], logits=pf_val[tt]) for \
            tt in range(num_segments)]
total_loss_val = tf.reduce_mean(losses_val)

def train_network(num_epochs, num_steps, verbose=True):
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        training_losses = []
        trX = embeddings
        for eid, epoch in enumerate(gen_epochs(num_epochs, num_steps)):
            training_loss = 0
            if verbose:
                print("\nEPOCH", eid)
            for step, trYb in enumerate(epoch):
                trid = trYb[0]
                trY = trYb[1]
                # training
                training_loss_, _ = sess.run([total_loss,train_step],feed_dict={idx:trid,cock:trX,sucker:trY})
                # eval on validation set
                validation_loss = 0.0
                for uu in range(n_val):
                    # prepare one-hot y
                    ohehoty = (np.tile(motif_val[uu,][:,np.newaxis],[1,num_classes]) == \
                            np.tile(np.arange(num_classes)[np.newaxis,],[num_segments,1])).astype(float)
                    validation_loss_, temp = sess.run([total_loss_val,pf_val],feed_dict={idx:val_ids[uu],cock:trX,sucker:ohehoty})
                    motif_val_pred[uu,:] = [np.argmax(i) for i in temp]
                    validation_loss += validation_loss_

                training_loss += training_loss_
                if step % 5 == 0 and step > 0:
                    if verbose:
                        print 'validation loss = ', validation_loss/n_val
                        print 'accuracy = ', np.sum(motif_val_pred == motif_val)
                        # print("Average loss at step", step,
                        #       "for last 250 steps:", training_loss/100)
                    training_losses.append(training_loss/100)
                    training_loss = 0

    return training_losses


np.random.seed(0)
training_losses = train_network(5,num_steps)
