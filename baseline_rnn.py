import pandas as pd
import numpy as np
import math
import tensorflow as tf
import matplotlib.pyplot as plt

def motif_map(motif):
    '''
    input: array numpy
    HE: 0
    LE: 1
    SB: 2
    SF:3
    Nan: 3 (another option: 4)
    '''
    index_array = ['HE','LE','SB','SF',np.nan]
    result = np.zeros([motif.shape[0], motif.shape[1]], dtype=np.int32)
    for iter_x in range(motif.shape[0]):
        for iter_y in range(motif.shape[1]):
            if motif[iter_x, iter_y] == 'RS': result[iter_x, iter_y] = 1
            else:result[iter_x,iter_y] = index_array.index(motif[iter_x,iter_y])
    np.place(result, result == 4, 1)
    return result

# Import data
motif = pd.read_csv('user_motif.csv') # segment output
motif = motif.iloc[:,1:] # remove first column index
embeddings = pd.read_csv('glove_embeddings.csv') # glove embeddings representing all text inputs
embeddings = embeddings.iloc[:,1:] # remove first column index
num_users = motif.shape[0]
num_segments = motif.shape[1]
motif = motif.as_matrix()
motif = motif_map(motif)

# get training and validation sets
np.random.seed(0)
pct_val = 0.01
n_val = int(np.round(num_users * pct_val))
perm = np.random.permutation(num_users)
val_ids = perm[0:n_val]
motif_val = motif[val_ids,]
motif_train = motif[perm[n_val:],]
num_users_train = num_users - n_val
motif_val_pred = np.zeros([n_val,num_segments])


# Global config variables
num_steps = 1 # number of truncated backprop steps ('n' in the discussion above), # of segment is 37 (prime)
batch_partition_length = motif.shape[1] # number of users to split the data to batches
num_classes = 4 # predicting HE/LE/SB/SF
state_size = embeddings.shape[0] # size of hidden state, k
learning_rate = 0.1 # for ADAMS


def get_batch(no_batches,motif_train):
    # partition raw data into batches and stack them vertically in a data matrix || y
    data_y = motif_train # .as_matrix()
    # data_y = motif_map(data_y)
    # further divide batch partitions into num_steps for truncated backprop
    N = motif_train.shape[0]

    for i in range(no_batches):
        randidx = np.random.permutation(N)[0]
        y_batch = data_y[randidx,]
        y_batch_full = (np.tile(y_batch[:,np.newaxis],[1,num_classes]) == \
                            np.tile(np.arange(num_classes)[np.newaxis,],[num_segments,1])).astype(float)
        # now turn into 1-hot
        yield (randidx,y_batch_full)

def gen_epochs(num_epochs, num_steps):
    for i in range(num_epochs):
        yield get_batch(num_steps, motif)


"""
Placeholders
"""

x = tf.placeholder(tf.float32, [embeddings.shape[0], embeddings.shape[1]], name='input_placeholder')
y = tf.placeholder(tf.float32, [embeddings.shape[1], num_classes], name='labels_placeholder')
init_state = tf.zeros([state_size, 1])

"""
RNN Inputs
"""

# Turn our x placeholder into a list of one-hot tensors:
# rnn_inputs is a list of num_steps tensors with shape [batch_size, num_classes]
rnn_inputs = tf.unstack(x, axis=1) # queue of x_batche


with tf.variable_scope('rnn_cell'):
    W = tf.get_variable('W', [state_size + state_size, state_size])
    b = tf.get_variable('b', [state_size], initializer=tf.constant_initializer(0.0))

def rnn_cell(rnn_input, state):
    with tf.variable_scope('rnn_cell', reuse=True):
        W = tf.get_variable('W', [state_size + state_size, state_size])
        b = tf.get_variable('b', [state_size], initializer=tf.constant_initializer(0.0))
    return tf.transpose(tf.tanh(tf.matmul(tf.transpose(tf.concat([tf.reshape(rnn_input,[embeddings.shape[0],1]), state], 0)), W) + b))


state = init_state
rnn_outputs = []
for rnn_input in rnn_inputs:
    state = rnn_cell(rnn_input, state)
    rnn_outputs.append(state)
final_state = rnn_outputs[-1]

#logits and predictions
with tf.variable_scope('softmax'):
    W = tf.get_variable('W', [state_size, num_classes])
    b = tf.get_variable('b', [num_classes], initializer=tf.constant_initializer(0.0))
logits = [tf.matmul(tf.transpose(rnn_output), W) + b for rnn_output in rnn_outputs]
predictions = [tf.nn.softmax(logit) for logit in logits]

# Turn our y placeholder into a list of labels
y_as_list = tf.unstack(y, num=embeddings.shape[1], axis=0)

#losses and train_step
losses = [tf.nn.softmax_cross_entropy_with_logits(labels=tf.cast(label,tf.int32), logits=tf.reshape(logit,[num_classes,])) for logit, label in zip(logits, y_as_list)]
total_loss = tf.reduce_mean(losses)
train_step = tf.train.AdagradOptimizer(learning_rate).minimize(total_loss)


def train_network(num_epochs, num_steps, state_size=state_size, verbose=True):
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        training_losses = []
        for idx, epoch in enumerate(gen_epochs(num_epochs, num_steps)):
            training_loss = 0
            training_state = np.zeros((state_size, 1))
            if verbose:
                print("\nEPOCH", idx)
            for step, (idx, Y) in enumerate(epoch):
                # training
                tr_losses, training_loss_, training_state, _ = sess.run([losses,total_loss,final_state,train_step],feed_dict={x:embeddings, y:Y, init_state:training_state})
                training_loss += training_loss_
                # print 'training loss = ', training_loss_
                training_losses.append(training_loss)
                training_loss = 0
                # validating
            validation_loss = 0.0
            for uu in range(n_val):
                ohehoty = (np.tile(motif_val[uu,][:, np.newaxis], [1, num_classes]) == np.tile(np.arange(num_classes)[np.newaxis,], [num_segments, 1])).astype(float)
                losses_uu = sess.run([total_loss],feed_dict={x: embeddings,y: ohehoty})
                validation_loss += losses_uu[0]
            print 'validation loss = ', validation_loss / n_val

    return training_losses


res = train_network(200, 200, state_size=state_size, verbose=True)
plt.plot(res) # plot the training loss over epoch



