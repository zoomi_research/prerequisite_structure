import matplotlib.pyplot as plt
t = np.arange(0, 300, 1)

# Create plots with pre-defined labels.
fig, ax = plt.subplots()
ax.plot(t, training_losses, 'k--', color = 'blue',label='Training Loss',linewidth = 1.5)
ax.plot(t, val_accuracy, 'k:', color = 'red', label='Validation Accuracy',linewidth = 1.5)
ax.plot(t, validation_loss, 'k', color = 'green', label='Validation Loss',linewidth = 1.5)

legend = ax.legend(loc='center right', shadow=True, fontsize='x-large')

# Put a nicer background color on the legend.
legend.get_frame()

plt.show()


