import pandas as pd
import numpy as np
import math
import tensorflow as tf
import matplotlib.pyplot as plt

def motif_map(motif):
    '''
    input: array numpy
    HE: 0
    LE: 1
    SB: 2
    SF:3
    Nan: 3 (another option: 4)
    '''
    index_array = ['HE','LE','SB','SF',np.nan]
    result = np.zeros([motif.shape[0], motif.shape[1]], dtype=np.int32)
    for iter_x in range(motif.shape[0]):
        for iter_y in range(motif.shape[1]):
            if motif[iter_x, iter_y] == 'RS': result[iter_x, iter_y] = 1
            else:result[iter_x,iter_y] = index_array.index(motif[iter_x,iter_y])
    np.place(result, result == 4, 1)
    return result

# Import data
motif = pd.read_csv('user_motif.csv') # segment output
motif = motif.iloc[:,1:] # remove first column index
embeddings = pd.read_csv('glove_embeddings.csv') # glove embeddings representing all text inputs
embeddings = embeddings.iloc[:,1:] # remove first column index
num_users = motif.shape[0]
num_segments = motif.shape[1]
motif = motif.as_matrix()
motif = motif_map(motif)

# get training and validation sets
np.random.seed(0)
pct_val = 0.01
n_val = int(np.round(num_users * pct_val))
perm = np.random.permutation(num_users)
val_ids = perm[0:n_val]
motif_val = motif[val_ids,]
motif_train = motif[perm[n_val:],]
num_users_train = num_users - n_val
motif_val_pred = np.zeros([n_val,num_segments])


# Global config variables
num_steps = 1 # number of truncated backprop steps ('n' in the discussion above), # of segment is 37 (prime)
batch_size = motif_train.shape[0] # number of segment as the number of the batch
batch_partition_length = motif.shape[1] # number of users to split the data to batches
num_classes = 4 # predicting HE/LE/SB/SF
state_size = embeddings.shape[0] # size of hidden state, k
learning_rate = 0.1 # for ADAMS


def get_batch(motif,embeddings,batch_size, num_steps):
    # partition raw data into batches and stack them vertically in a data matrix || y
    # data_y = motif.as_matrix()
    # data_y = motif_map(data_y)
    # further divide batch partitions into num_steps for truncated backprop
    epoch_size = batch_partition_length // num_steps

    for i in range(epoch_size):
        x_batch = embeddings.as_matrix()
        x_batch_ = x_batch[:, i * num_steps:(i + 1) * num_steps].transpose() # 100 X 1 vector
        x_batch = np.repeat(x_batch_[np.newaxis, :, :], batch_size, axis=0) # num_user X 1 X 100
        y_batch = motif[:, i * num_steps:(i + 1) * num_steps] # batch_size X num_steps
        #print y_batch
        yield (x_batch, y_batch)

def gen_epochs(n, num_steps):
    for i in range(n):
        yield get_batch(motif_train,embeddings, batch_size, num_steps)


"""
Placeholders
"""

x = tf.placeholder(tf.int32, [batch_size, num_steps], name='input_placeholder')
y = tf.placeholder(tf.int32, [batch_size, num_steps], name='labels_placeholder')
init_state = tf.zeros([batch_size, state_size])

"""
RNN Inputs
"""

# Turn our x placeholder into a list of one-hot tensors:
# rnn_inputs is a list of num_steps tensors with shape [batch_size, num_classes]
x_one_hot = tf.one_hot(x, state_size) # shape of batch_size X num_steps X state size (dimension of glove)
rnn_inputs = tf.unstack(x_one_hot, axis=1) # queue of x_batche, queue length = num_steps, batch_size X state_size


with tf.variable_scope('rnn_cell'):
    W = tf.get_variable('W', [state_size + state_size, state_size])
    b = tf.get_variable('b', [state_size], initializer=tf.constant_initializer(0.0))

def rnn_cell(rnn_input, state):
    with tf.variable_scope('rnn_cell', reuse=True):
        W = tf.get_variable('W', [state_size + state_size, state_size])
        b = tf.get_variable('b', [state_size], initializer=tf.constant_initializer(0.0))
    return tf.tanh(tf.matmul(tf.concat([rnn_input, state], 1), W) + b)


state = init_state
rnn_outputs = []
for rnn_input in rnn_inputs:
    state = rnn_cell(rnn_input, state)
    rnn_outputs.append(state)
final_state = rnn_outputs[-1]

#logits and predictions
with tf.variable_scope('softmax'):
    W = tf.get_variable('W', [state_size, num_classes])
    b = tf.get_variable('b', [num_classes], initializer=tf.constant_initializer(0.0))
logits = [tf.matmul(rnn_output, W) + b for rnn_output in rnn_outputs]
predictions = [tf.nn.softmax(logit) for logit in logits]

# Turn our y placeholder into a list of labels
y_as_list = tf.unstack(y, num=num_steps, axis=1)

#losses and train_step
losses = [tf.nn.sparse_softmax_cross_entropy_with_logits(labels=label, logits=logit) for \
          logit, label in zip(logits, y_as_list)]
total_loss = tf.reduce_mean(losses)
train_step = tf.train.AdagradOptimizer(learning_rate).minimize(total_loss)

def train_network(num_epochs, num_steps, state_size=state_size, verbose=True):
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        training_losses = []
        for idx, epoch in enumerate(gen_epochs(num_epochs, num_steps)):
            training_loss = 0
            training_state = np.zeros((batch_size, state_size))
            if verbose:
                print("\nEPOCH", idx)
            for step, (X, Y) in enumerate(epoch):
                # training
                tr_losses, training_loss_, training_state, _ = \
                    sess.run([losses,
                              total_loss,
                              final_state,
                              train_step],
                                  feed_dict={x_one_hot:X, y:Y, init_state:training_state})
                training_loss += training_loss_
                # validating

                if step % 10 == 0 and step > 0:
                    if verbose:
                        print("Average loss at step", step,
                              "for last 250 steps:", training_loss/100)
                    training_losses.append(training_loss/100)
                    training_loss = 0

    return training_losses


res = train_network(1, num_steps, state_size=state_size, verbose=False)
plt.plot(res) # plot the training loss over epoch






