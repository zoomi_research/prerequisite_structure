import pandas as pd
import pickle
import csv


def find_content_length():

    map = {
        "ITM_GL_GL_RRP_030_1491403560000_46581": '1_what_is_pmis_vision_for_rrps',
        "ITM_GL_GL_RRP_030_1491403560000_46582": "2_why_are_we_developing_rrps",
        "ITM_GL_GL_RRP_030_1491403560000_46583": "4_what_is_pmis_rrp_portfolio",
        "ITM_GL_GL_RRP_030_1491403560000_46584": "3_what_is_the_basic_principle_of_heatnotburn_platforms",
    }

    content_length = {c:0 for c in map}

    df = pd.read_csv('glove_embeddings.csv', index_col=0)

    for name in list(df.columns.values):
        for contentID, text in map.items():
            if text in name:
                content_length[contentID] += 1
                break

    return content_length



def make_u_by_segment_matrix(raw, output_name, sub_val):
    users = raw.keys()

    content_lengths = find_content_length()

    correct_c_names = list( set([c for u in raw for c in raw[u]]) )


    for c in content_lengths:
        for cc in correct_c_names:
            if c in cc:
                content_lengths[cc] = content_lengths[c]
                content_lengths[cc] = content_lengths.pop(c)
                break

    print(content_lengths)

    segment_names = []
    contents = sorted( content_lengths.keys() )
    for c in contents:
        for i in range(0, content_lengths[c]):
            segment_names.append(c + "_" + str(i))


    data = []
    for u in users:
        row_u = []
        for c in contents:
            len_c = content_lengths[c]
            if c in raw[u]:
                # if len(raw[u][c]) != len_c:
                    # print(u, c, len_c, len(raw[u][c]))

                if len(raw[u][c]) >= len_c:
                    row_u += raw[u][c][0:len_c]
                else:
                    row_u += raw[u][c] + [sub_val]*(len_c - len(raw[u][c]))

            else:
                row_u += ['NA'] * len_c

        data.append(row_u)

    df = pd.DataFrame(data=data, index=users, columns=segment_names)
    df.to_csv(output_name)

    return 0


raw = pickle.load( open( "outcome_variable_ITM_GL_GL_RRP_030_1491403560000.pickle", "rb" ) )
make_u_by_segment_matrix(raw, 'u_by_segment_motif.csv', 'LE')
raw = pickle.load( open( "ITM_GL_GL_RRP_030_1491403560000_engagement_gate.pickle", "rb" ) )
make_u_by_segment_matrix(raw, 'u_by_segment_eng_gate.csv', 0)



