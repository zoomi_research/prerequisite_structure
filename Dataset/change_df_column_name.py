import pandas as pd

map = {
    "ITM_GL_GL_RRP_030_1491403560000_46581" : '1_what_is_pmis_vision_for_rrps',
    "ITM_GL_GL_RRP_030_1491403560000_46582" : "2_why_are_we_developing_rrps",
    "ITM_GL_GL_RRP_030_1491403560000_46583" : "4_what_is_pmis_rrp_portfolio",
    "ITM_GL_GL_RRP_030_1491403560000_46584" : "3_what_is_the_basic_principle_of_heatnotburn_platforms",
}


df = pd.read_csv('glove_embeddings.csv', index_col=0)

for name in list(df.columns.values):

    c = ''
    for contentID, text in map.items():
        if text in name:
            c = contentID
            break

    tail = name.split('_')[-1][:-4]

    try:
        tail = int(tail)
        new_name = c + "_" + str(tail)
    except:
        new_name = c

    # if isinstance(tail, int):
    #     new_name = c + "_" + tail
    # else:
    #     new_name = c

    print(name, '       ', new_name)

    df.rename(columns={name:new_name}, inplace=True)




df.to_csv('Glove_Embeddings_new.csv')