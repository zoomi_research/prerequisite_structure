__author__ = 'WeiyuChen'
import pandas as pd
from database_connect import *
import pickle
import numpy as np
import save_motifs_to_json_video
import feature_based_motif_to_json_video
import feature_action_sequence_combination
import search_motifs_video
import courses
import toCombine
import combine_repetitive
import save_motifs_to_json_storyline

def user_course(datamart_connection,course_ID):
    course_collection = datamart_connection['prod_datamart']['user_content_interactions']
    return list(course_collection.find({'course':course_ID}).distinct('user'))

''' run '''
def main():
    '''connecting to AWS'''
    connection = mongo_connect()
    print ' mongo backend connected '
    datamart_connection = mongo_datamart_connect()
    print ' mongo datamart connected '
    sql_connection = mysql_connect()
    print ' sql backend connected '

    courses = list(datamart_connection['prod_datamart']['content_interactions'].distinct('course'))
    number_content = []
    number_user = []
    for course in courses:
        if 'ITM' in course:
            print course
            #events = list(datamart_connection['prod_datamart']['content_interactions'].find({'course':course}))
            contents = list(datamart_connection['prod_datamart']['content_interactions'].find({'course':course}).distinct('content'))
            number_content.append(len(contents))
            users = list(datamart_connection['prod_datamart']['content_interactions'].find({'course':course}).distinct('users'))
            number_user.append(len(users))