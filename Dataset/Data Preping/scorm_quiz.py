__author__ = 'WeiyuChen'
import pandas as pd
import pymongo
from database_connect import *
from configuration import *
from StringIO import StringIO
import json
import logging
import pickle

def user_course_list(mysql, collection, course):
    if isinstance(course, basestring):
        courses = [course]
    else:
        courses = course
    # Get all of the valid users with their external id
    cursor = mysql.cursor()
    sql = 'SELECT id,externalID,user_id FROM user_management_miicuser'
    cursor.execute(sql)
    result = cursor.fetchall()
    valid_user_ids = []
    for res in result:
        valid_user_ids.append(res[0])
    user_df = pd.DataFrame([[ij for ij in i] for i in result])
    user_df.rename(columns={0: "user_id", 1: "external_id", 2: "auth_id"}, inplace=True)

    user_course_df = []
    # Get the users with clickstream events in measure_base
    for course in courses:
        users = collection.find({'course': course}).distinct('user')
        valid_course_users = set(users).intersection(valid_user_ids)
        user_records = [(user, course) for user in valid_course_users]
        labels = ['user_id', 'course_id']
        if len(user_course_df) > 0:
            user_course_df = pd.concat([user_course_df, pd.DataFrame.from_records(user_records, columns=labels)])
        else:
            user_course_df = pd.DataFrame.from_records(user_records, columns=labels)
    # get the external id
    final_user_df = pd.merge(user_course_df, user_df, how='left', on='user_id')

    # For quiz completion, we should also be adding first and last name
    cursor = mysql.cursor()
    sql = 'SELECT id,first_name,last_name FROM auth_user'
    cursor.execute(sql)
    result = cursor.fetchall()
    user_info_df = pd.DataFrame([[ij for ij in i] for i in result])
    user_info_df.rename(columns={0: "auth_id", 1: "first_name", 2: "last_name"}, inplace=True)
    final_user_info_df = pd.merge(final_user_df, user_info_df, how='left', on='auth_id')
    return final_user_info_df, len(valid_course_users)

def populate_quiz_data(user_by_course, collection, course):
    questions = collection.find({'course': course}).distinct('data.activityDetails.children.runtime.runtimeInteractions.id')
    user_by_course["quiz_status"] = pd.Series(['N/A' for i in range(0, len(user_by_course['course_id']))])
    user_by_course["quiz_overall_score"] = pd.Series(['N/A' for i in range(0, len(user_by_course['course_id']))])
    for question in questions:
        user_by_course[question + "_response"] = pd.Series(['N/A' for i in range(0, len(user_by_course['course_id']))])
        user_by_course[question + "_result"] = pd.Series(['N/A' for i in range(0, len(user_by_course['course_id']))])
    print user_by_course
    for user in user_by_course.user_id.unique():
        user_quiz_data = collection.find({'course': course,
                                          "user": user,
                                          "data.activityDetails.children.runtime.runtimeInteractions": {'$exists': True}}).sort("data.updated", -1)
        latest_update = list(user_quiz_data)[0]

        user_by_course.loc[(user_by_course.user_id == user), 'quiz_status'] = latest_update['data']['activityDetails']['children'][0]['runtime']['completionStatus']
        user_by_course.loc[(user_by_course.user_id == user), 'quiz_overall_score'] = latest_update['data']['activityDetails']['children'][0]['runtime']['scoreScaled']
        for question in latest_update['data']['activityDetails']['children'][0]['runtime']['runtimeInteractions']:
            user_by_course.loc[(user_by_course.user_id == user), question['id'] + '_response'] = question.get('learnerResponse', 'N/A')
            user_by_course.loc[(user_by_course.user_id == user), question['id'] + '_result'] = question['result']
    return user_by_course

def main(course_ID):
    '''connecting to AWS'''
    connection = mongo_connect()
    print ' mongo backend connected '
    datamart_connection = mongo_datamart_connect()
    print ' mongo datamart connected '
    sql_connection = mysql_connect()
    print ' sql backend connected '


    #course_ID = 'ITM_GL_GL_RRP_030_1491403560000'
    scorm_collection = connection['prod_backend']['scorm_registration_progress']

    user_by_course, num_valid_users = user_course_list(sql_connection, scorm_collection, course_ID)
    quiz_df = populate_quiz_data(user_by_course, scorm_collection, course_ID)

    quiz_result = quiz_df.iloc[:, 0:8]
    #quiz_result.to_csv(course_ID + '_quiz.csv')

    with open('/Users/weiyuchen/Documents/Zoomi Research/Data Analysis/Feature Extraction/Behavior Modeling/Local Data Copy/quiz'+course_ID, 'wb') as fp:
        pickle.dump(quiz_result, fp)

    return quiz_df.iloc[:,0:8]



