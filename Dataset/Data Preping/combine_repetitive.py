
def main(action_sequence):
    users = action_sequence.keys()
    for user in users:
        items = action_sequence[user].keys()
        for item in items:
            for iter in range(len(action_sequence[user][item][0]) - 1):
                if action_sequence[user][item][0][iter]['action'] == action_sequence[user][item][0][iter + 1]['action']:
                    action_sequence[user][item][0][iter]['time_elapsed'] = action_sequence[user][item][0][iter][
                                                                               'time_elapsed'] + \
                                                                           action_sequence[user][item][0][iter + 1][
                                                                               'time_elapsed']
                    del action_sequence[user][item][0][iter + 1]

    return action_sequence
