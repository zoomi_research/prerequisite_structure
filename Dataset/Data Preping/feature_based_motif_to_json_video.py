__author__ = 'WeiyuChen'
import json
import numpy as np
import math
from itertools import groupby

# input
segment_length = 20
high_thres = 1
low_thres = 0.3

def encoding_rle(s):
    # run length encoder
    return "".join((k + str(len(list(g)))) for k, g in groupby(s))

def content_expected_time_spent(mongo_connection,users,course_ID):
    chapter_IDs = list(mongo_connection['prod_backend']['measure_base'].find({'course': course_ID}).distinct('chapter'))
    chapter_expected_time_spent = []
    for chapter_ID in chapter_IDs:
        per_chapter_ETS = []
        clickstream_events = list(mongo_connection['prod_backend']['measure_base'].find({'course': course_ID,'chapter':chapter_ID}))
        for clickstream_event in clickstream_events:
            if clickstream_event['measurementType']=='video':
                per_chapter_ETS.append(clickstream_event['duration'])
            else:
                per_chapter_ETS.append(float(0))
        chapter_expected_time_spent.append(max(per_chapter_ETS))
    return chapter_IDs,chapter_expected_time_spent

def per_content_ETS_segment(per_content_ETS,segment_length):
    # index all 1-second intervals to corresponding 20 second segments
    number_of_segment = math.ceil(per_content_ETS / segment_length)
    index_segment = [range(segment_length*(a-1),segment_length*a) for a in range(1, int(number_of_segment))]
    index_segment.append(range(int(segment_length*(number_of_segment-1)),int(math.ceil(per_content_ETS))+1))
    return index_segment

def missing_elements(L):
    start, end = L[0], L[-1]
    return sorted(set(range(start, end + 1)).difference(L))

def main(datamart,course_ID,users,mongo_connection):
    course_collection = datamart['prod_datamart']['user_segment_interactions']
    # define content length
    chapter_IDs, chapter_expected_time_spent = content_expected_time_spent(mongo_connection, users, course_ID)

    # define storage unit
    feature_based_action_sequence = {}

    for user in users:
        # for every user
        items = list(course_collection.find({'course': course_ID, 'user': user}))
        feature_based_action_sequence[user] = {}
        if len(items) > 0:
            content_IDs = list(course_collection.find({'course': course_ID, 'user': user}).distinct('content'))
            for content_ID in content_IDs:
                feature_based_action_sequence[user][content_ID] = []
                interval_engagement_per_segment = []
                interval_view_count_per_segment = []
                per_user_per_content_items = list(
                    course_collection.find({'course': course_ID, 'user': user, 'content': content_ID}))
                per_content_ETS = chapter_expected_time_spent[
                    [a + '_CONTENT0' for a in chapter_IDs].index(content_ID)]  # expected time spent for this content
                interval_visited_engagement = [0] * int(per_content_ETS + 1)
                interval_visited_view_count = [0] * int(per_content_ETS + 1)
                for per_user_per_content_item in per_user_per_content_items:
                    interval_visited_engagement[per_user_per_content_item['segment']] = per_user_per_content_item['segment_engagement']
                    interval_visited_view_count[per_user_per_content_item['segment']] = int(per_user_per_content_item['times_viewed'])
                ''' deal with engagement '''
                # cap engagement
                interval_visited_engagement = [min(1, a) for a in interval_visited_engagement]
                # group to 20second intervals
                while len(interval_visited_engagement) > 0:
                    interval_engagement_per_segment.append(np.mean(interval_visited_engagement[0:segment_length]))
                    del interval_visited_engagement[0:segment_length]
                # group by similar HE or LE
                interval_visited_engagement_string = encoding_rle(
                    ["HE" if a >= high_thres else "LE" for a in interval_engagement_per_segment])
                interval_visited_engagement_string = [''.join(g) for _, g in
                                                      groupby(interval_visited_engagement_string, str.isalpha)]
                # group final segment engagement values to dictionary
                temp_action_sequence = []
                while len(interval_visited_engagement_string) > 0:
                    temp_action_sequence.append({'action': interval_visited_engagement_string[0], 'time_elapsed': int(
                        interval_visited_engagement_string[1])})
                    del interval_visited_engagement_string[0:2]
                ''' deal with view counts '''
                # group to 20second intervals
                while len(interval_visited_view_count) > 0:
                    interval_view_count_per_segment.append(np.mean(interval_visited_view_count[0:segment_length]))
                    del interval_visited_view_count[0:segment_length]
                if any(t > 1 for t in interval_view_count_per_segment):
                    feature_position = list(np.cumsum([x['time_elapsed'] for x in temp_action_sequence]))
                    # group by repeating a segment or not (repeating view count)
                    interval_visited_view_count_string = encoding_rle(
                        ["RS" if a > 1 else "NS" for a in interval_view_count_per_segment])
                    interval_visited_view_count_string = [''.join(g) for _, g in
                                                          groupby(interval_visited_view_count_string, str.isalpha)]
                    # group final segment engagement values to dictionary
                    accumulative_timestamp = 0
                    while len(interval_visited_view_count_string) > 0:
                        accumulative_timestamp = accumulative_timestamp +int(interval_visited_view_count_string[1])
                        if interval_visited_view_count_string[0]=='RS':
                            to_insert_index = [accumulative_timestamp <= feature_pos for feature_pos in feature_position].index(True)  # find the index to insert
                            temp_action_sequence.insert(to_insert_index, {'action':'RS','time_elapsed':int(interval_visited_view_count_string[1])})
                        del interval_visited_view_count_string[0:2]

                print temp_action_sequence
                feature_based_action_sequence[user][content_ID].append(temp_action_sequence)

    with open('users_videoIDs_feature_sequences.json', 'w') as fp:
        json.dump(feature_based_action_sequence, fp)

    return feature_based_action_sequence



