__author__ = 'WeiyuChen'

# this code cleans out empty lists [] embedded within the dictionary
# also, controls whether if we want to group timestamp/# segments associated with actions in the sequence

import pickle

with open('action_sequence_combination', 'rb') as fp:
    action_sequence = pickle.load(fp)

def remove_empty_list(action_sequence):
    users = action_sequence.keys()
    new_action_sequence = {}
    for user in users:
        new_action_sequence[user] = {}
        contents = action_sequence[user].keys()
        for content in contents:
            if action_sequence[user][content][0] != []:
                new_action_sequence[user][content] = []
                new_action_sequence[user][content].append(action_sequence[user][content][0])
    return new_action_sequence

def group_segment(action_sequence):

    users = action_sequence.keys()
    new_action_sequence = {}
    for user in users:
        contents = action_sequence[user].keys()
        new_action_sequence[user] = {}
        for content in contents:
            #print action_sequence[user][content]
            print [x['action'] for x in action_sequence[user][content][0]]
            print [x['time_elapsed'] for x in action_sequence[user][content][0]]






    print 1
    return 1

def main(action_sequence):
    new_action_sequence = remove_empty_list(action_sequence)

    return new_action_sequence
