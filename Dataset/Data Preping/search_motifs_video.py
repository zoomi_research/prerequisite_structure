__author__ = 'WeiyuChen'

import json
from itertools import product
import pickle
import pandas as pd

class Mnode():
    def __init__(self, act):
        self.act = act
        self.count = 0
        self.depth = -1
        self.isEnd = False
        self.kids = {} # a dictionary of child nodes

class Tree():
    def __init__(self):
        self.L = []
        self.root = Mnode('root')
        self.root.depth = 0
        self.dict_m_count = {}

    def insert(self, seq): # put a subsequence into the tree
        node = self.root
        node.count += 1
        for act in seq:
            if act not in node.kids:
                node.kids[act] = Mnode(act)
            current_depth = node.depth
            node = node.kids.get(act)
            node.count += 1
            node.depth = current_depth + 1
        node.isEnd = True
        return 0

    def count_all_motifs(self):
        self.dict_m_count = {}
        node = self.root
        for act in node.kids:
            new_node = node.kids.get(act)
            self.trace(new_node, [])

    def trace(self, node, motif):
        if node.isEnd:
            m = '_'.join(motif+[node.act])
            self.dict_m_count[m] = node.count
            return 0
        else:
            for kid in node.kids:
                kid_node = node.kids.get(kid)
                self.trace(kid_node, motif+[node.act])


    def check_count(self, seq):
        node = self.root
        for act in seq:
            if act not in node.kids:
                return False
            node = node.kids[act]
        return node.count

    def delete(self, seq):
        node = self.root
        # for act in seq:




def sequence_segmentation_and_erasingFactor(W, Y):
    # Y is a list of lists, with each list being the behavior sequence of a user
    # W is an integer, length of motif
    X = []
    for n, y in enumerate(Y):
        if len(y) < W:
            continue
        for i in range(0, len(y) - W + 1):
            tmp = y[i:i + W]
            X.append(tmp)

    return X


def main_search_for_user_count(dict_u_content_sequences):

    # with open('action_sequence_combination', 'rb') as fp:
    #     dict_u_content_sequences = pickle.load(fp)

    all_seq = []
    for u in dict_u_content_sequences:
        for content in dict_u_content_sequences[u]:
            all_seq += dict_u_content_sequences[u][content]

    for i, seq in enumerate(all_seq):
        print seq
        all_seq[i] = [x['action'] + str(int(x['time_elapsed']))for x in seq]
        #all_seq[i] = [x['action'] for x in seq] # do not consider number of segments

    motif_length = 3
    X = sequence_segmentation_and_erasingFactor(motif_length, all_seq)

    motif_tree = Tree()
    for x in X:
        motif_tree.insert(x)
    motif_tree.count_all_motifs()
    m_count = motif_tree.dict_m_count
    motifs = m_count.keys()
    del motif_tree




    dict_u_seqs = {}
    for u in dict_u_content_sequences:
        dict_u_seqs[u] = []
        for content in dict_u_content_sequences[u]:
            dict_u_seqs[u] += dict_u_content_sequences[u][content]

    dict_u_motif_counts = {}
    for u in dict_u_seqs:
        for i, seq in enumerate(dict_u_seqs[u]):
            dict_u_seqs[u][i] = [x['action'] + str(int(x['time_elapsed'])) for x in seq]
            #dict_u_seqs[u][i] = [x['action'] for x in seq] # remove number of segments considered
            print 1

        X = sequence_segmentation_and_erasingFactor(motif_length, dict_u_seqs[u])
        motif_tree = Tree()
        for x in X:
            motif_tree.insert(x)
        motif_tree.count_all_motifs()
        dict_u_motif_counts[u] = motif_tree.dict_m_count
        #print dict_u_motif_counts[u]
        del motif_tree

    df = pd.DataFrame.from_dict(dict_u_motif_counts, orient='index')
    df = df.fillna(value=0)
    # with open('motifs.csv', 'w') as f:
    #     df.to_csv(f)


    return df
