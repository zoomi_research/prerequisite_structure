import numpy as np
import json
import pickle
from itertools import groupby
import copy

segment_length = 20

# with open('users_videoIDs_sequences.json') as data_file:
#     tocombine_event_based_action_sequence = json.load(data_file)
#
# with open('users_videoIDs_feature_sequences.json') as data_file:
#     tocombine_feature_based_action_sequence = json.load(data_file)

def main(tocombine_event_based_action_sequence,tocombine_feature_based_action_sequence):
    action_sequence = {}
    ''' grab all users '''
    users = tocombine_feature_based_action_sequence.keys()
    for user in users:
        action_sequence[user] = {}
        events = tocombine_event_based_action_sequence[user]
        features = tocombine_feature_based_action_sequence[user]
        contents = list(set(events.keys()+features.keys()))
        for content in contents:
            if not features:
                continue
            if not content in features.keys():
                continue
            action_sequence[user][content] = []
            action_sequence[user][content].append(events[content][0])
            #print user,content,action_sequence[user][content]

            feature_position = list(np.cumsum([x['time_elapsed']*segment_length for x in features[content][0]]))
            action_position = [x['position'] for x in events[content][0]]

            # add position to feature based motif
            for iter, temp_feature_action_sequence in enumerate(features[content][0]):
                temp_feature_action_sequence['position'] = feature_position[iter]

            # break feature based motif to finer segments
            for action_pos in action_position:
                if True in [ex > action_pos for ex in feature_position]:
                    todivide_index = [ex > action_pos for ex in feature_position].index(True)
                    action = features[content][0][todivide_index]['action']
                    features[content][0][todivide_index]
                    position1 = action_pos + 0.00001  # end position for this feature action sequence, with near-zero number for easier ranking
                    time_elapsed1 = (action_pos - (
                    features[content][0][todivide_index]['position'] - features[content][0][todivide_index][
                        'time_elapsed'] * segment_length)) / segment_length
                    position2 = features[content][0][todivide_index]['position']
                    time_elapsed2 = (features[content][0][todivide_index]['position'] - action_pos) / segment_length
                    # insert and edit newly created finer segments
                    features[content][0][todivide_index]['action'] = action
                    features[content][0][todivide_index]['position'] = position1
                    features[content][0][todivide_index]['time_elapsed'] = time_elapsed1
                    to_insert_sequence = {'action': action, 'position': position2, 'time_elapsed': time_elapsed2}
                    features[content][0].insert(todivide_index + 1, to_insert_sequence)
                    feature_position = list(
                        np.cumsum([x['time_elapsed'] * segment_length for x in features[content][0]]))




            for iter,temp_feature_action_sequence in enumerate(features[content][0]):
                if not True in [ex['position'] > feature_position[iter] for ex in action_sequence[user][content][0]]:
                    action_sequence[user][content][0].append(temp_feature_action_sequence)
                else:
                    to_insert_index = [ex['position'] > feature_position[iter] for ex in action_sequence[user][content][0]].index(True) # find the index to insert
                    action_sequence[user][content][0].insert(to_insert_index, temp_feature_action_sequence)


            motherfuck = copy.copy(action_sequence[user][content])
            index_to_delete = []
            for iter in range(0,len(action_sequence[user][content][0])-1):
                # because previously we divide feature action sequences to finer levels to insert event sequences
                # certain sequences need to be combined back together, for repetitive same sequences
                if action_sequence[user][content][0][iter]['action'] == action_sequence[user][content][0][iter+1]['action']:
                    action_sequence[user][content][0][iter+1]['time_elapsed'] = action_sequence[user][content][0][iter+1]['time_elapsed'] + action_sequence[user][content][0][iter]['time_elapsed']
                    index_to_delete.append(iter)
            for index in sorted(index_to_delete, reverse=True):
                del action_sequence[user][content][0][index]


            # # delete every action sequence after CS (switching content)
            # index_to_delete_CS = []
            # CS_happen = False
            # for iter in range(0, len(action_sequence[user][content][0])):
            #     if CS_happen:
            #         index_to_delete_CS.append(iter)
            #     if action_sequence[user][content][0][iter]['action'] == 'CS':
            #         CS_happen = True
            # for index in sorted(index_to_delete_CS, reverse=True):
            #     del action_sequence[user][content][0][index]

            #action_sequence[user][content].append(temp_action_sequences)

    # with open('action_sequence_combination', 'wb') as fp:
    #     pickle.dump(action_sequence, fp)

    return action_sequence


def group_segment(action_sequence):
    users = action_sequence.keys()
    new_action_sequence = {}
    for user in users:
        contents = action_sequence[user].keys()
        new_action_sequence[user] = {}
        for content in contents:
            per_user_per_content_sequences = action_sequence[user][content][0]
            #print per_user_per_content_sequences
            for per_user_per_content_sequence in per_user_per_content_sequences:
                print per_user_per_content_sequence
                if per_user_per_content_sequence['time_elapsed']<=5:
                    per_user_per_content_sequence['time_elapsed'] = 1
                else:
                    per_user_per_content_sequence['time_elapsed'] = 2

    return action_sequence