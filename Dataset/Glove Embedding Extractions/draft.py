import json
import sys
import pandas as pd
import extract_text

def retrieve_navigation_data(course):
    # the frame.js file contains data regarding the relative ordering of slides
    # This will be used to get the underlying order of the course
    with open("courses/{course}/html5/data/js/data.js".format(course=course)) as js_file:
        content = "".join(js_file.readlines())

    # Get rid of all of the garbage such that we can extract the JSON
    content = content.split("'data', ")[1]
    content = content[1:-3]
    content_json = fix_JSON(content)
    all_slides = []
    for scene in content_json['scenes']:
        if 'slides' in scene:
            for slide in scene['slides']:
                if slide['navIndex'] >= 0:
                    all_slides.append(slide)
    all_slides = sorted(all_slides,key=lambda slide: slide['navIndex'])
    all_slides = map(lambda slide: (slide['navIndex'],slide['id'],slide['title'],slide['html5url']),all_slides)
    return all_slides,content_json


def join_with_assets(nav_data,content_json):
    df = pd.DataFrame(nav_data, columns=['navIndex', 'id', 'title', 'html5url'])
    df['audio'] = ''
    df['video'] = ''
    slide_refs = content_json['slideMap']['slideRefs']
    assetLib = content_json['assetLib']
    for index, row in df.iterrows():
        id = row['id']
        for slide in slide_refs:
            if '.' in slide['id'] and slide['id'].split('.')[1] == id:
                assets = slide['assetIds']
                for asset in assets:
                    cur_asset = assetLib[asset]
                    if 'mp3' in cur_asset['url']:
                        if len(row['audio']) == 0:
                            df.loc[index ,'audio'] = cur_asset['url']
                        else:
                            df.loc[index, 'audio'] = cur_asset['url'] + ',' + df.loc[index ,'audio']
                    elif 'mp4' in cur_asset['url']:
                        if len(row['video']) == 0:
                            df.loc[index, 'video'] = cur_asset['url']
                        else:
                            df.loc[index, 'video'] = cur_asset['url'] + ',' + df.loc[index, 'video']
    print(df)
    return df

def fix_JSON(json_message=None):
    result = None
    try:
        result = json.loads(json_message)
    except Exception as e:
        # Find the offending character index:
        idx_to_replace = int(e.message.split(' ')[-1].replace(')',''))
        # Remove the offending character:
        json_message = list(json_message)
        json_message[idx_to_replace] = ' '
        new_message = ''.join(json_message)
        return fix_JSON(json_message=new_message)
    return result



if __name__ == '__main__':
    sys.setrecursionlimit(2000)
    courses = ['CAP210','INV121']
    #courses = ['INV121']
    for course in courses:
        nav_data,content_json = retrieve_navigation_data(course)
        content_by_slide = join_with_assets(nav_data,content_json)
        #extract_text.extract_audio_video(content_by_slide,course)
        #extract_text.alter_course_names(course)