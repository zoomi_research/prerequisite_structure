__author__ = 'ricedspgroup'
# this code implements the Behavior Prereq RNN class
# assume it's one video for now

import numpy as np
import scipy.io as sio

def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))

def tanh(x):
    return (np.exp(x) - np.exp(-x)) / (np.exp(x) + np.exp(-x))

def softmax(x):
    denom = np.sum(np.exp(x))
    return np.exp(x) / denom

class RNN:

    def __init__(self, X, Y, hidden_dim=10, alpha=0.01, no_epoch=1000):

        # X has size glove x segment
        self.X = X # X is #.glove x #.segments
        # self.F = F
        self.Y = Y # Y is #.user x #.segments
        self.S = X.shape[1] # number of segments
        self.D = X.shape[0] # number of users
        # f is the action sequences of users
        self.K = hidden_dim # hidden dimension size
        self.A = 4 # 4 total actions (HE([0])/LE([1])/SF([2])/SB([3]))
        # F is users x #.segments
        self.N = Y.shape[0] # number of users
        self.alpha = alpha # learning rate?
        self.epoch = no_epoch # number of epoch
        # Randomly initialize the network parameters
        # input,output,transition matrices
        self.U = np.random.uniform(-1/np.sqrt(self.D), 1/np.sqrt(self.D), size=(self.K,self.D))
        self.V = np.random.uniform(-1/np.sqrt(self.A), 1/np.sqrt(self.A), size=(self.A,2 * self.K))
        self.W = np.random.uniform(-1/np.sqrt(self.K), 1/np.sqrt(self.K), size=(self.K,self.K))
        # target knowledge level for each user
        self.g = 0.01 * np.random.normal(size=(self.K,self.N))
        # prereq knowledge level for each segment
        self.p = 0.01 * np.random.normal(size=(self.K,self.S))
        # initialize the prerequisite matrix to lower-triangular
        self.R = np.tril(0.1 * np.random.uniform(size=(self.S,self.S)),-1)

    def fp(self,uid):
        # forward propagation
        T = self.S # equal to #.segments for now
        # the entire latent state
        h = np.zeros((self.K,T))
        # store the predictive probability
        pf = np.zeros((self.A,T))
        # the latent engagement scalar
        e = np.zeros((T,))
        # another variable recording the knowledge learned so far in each segment
        hl = np.zeros((self.K,T))
        g = np.zeros((self.K,T))
        w = np.zeros((self.K,T))

        # do the first time index first
        # h[:,0] is all zero - since nothing in yet
        w[:,0] = self.g[:,uid] - h[:,0] # destination knowledge gap
        g[:,0] = self.p[:,0] # prerequisite knowledge gap, rt is all zero since nothing in yet
        gwt = np.hstack((g[:,0],w[:,0]))
        pf[:,0] = softmax(np.dot(self.V,gwt))
        e[0] = softmax(np.dot(self.V[0:2,],gwt))[0]
        # learned knowledge in each segment
        hl[:,0] = e[0] * np.dot(self.U,self.X[:,0])

        # then do the rest
        for tt in range(1,T):
            # first get the current latent state
            h[:,tt] = np.dot(self.W,h[:,tt-1]) + hl[:,tt-1]
            w[:,tt] = self.g[:,uid] - h[:,tt]
            # now calculate the gap using prerequite matrix R
            g[:,tt] = self.p[:,tt]
            for t in range(tt-1,-1,-1):
                g[:,tt] -= self.R[t,tt] * hl[:,t]
            gwt = np.hstack((g[:,tt],w[:,tt]))
            pf[:,tt] = softmax(np.dot(self.V,gwt))
            e[tt] = softmax(np.dot(self.V[0:2,],gwt))[0]
            hl[:,tt] = e[tt] * np.dot(self.U,self.X[:,tt])

        return (h,hl,pf,g,w,e)

    def loss_1pt(self,uid):
        # calculate loss for 1 data pt
        (h,hl,pf,g,w,e) = self.fp(uid)
        return np.sum(-np.log(pf[self.Y[uid,],np.arange(self.S)]))

    def backprop(self,uid):
        # backprop derivative calc
        T = self.S
        (h,hl,pf,g,w,e) = self.fp(uid)
        dU = np.zeros((self.K,self.D))
        dV = np.zeros((self.A,2 * self.K))
        dW = np.zeros((self.K,self.K))
        dp = np.zeros((self.K,self.S))
        dg = np.zeros((self.K,self.N))
        dR = np.zeros((self.S,self.S))

        # another key variable
        de = np.zeros((self.S,))

        o1 = pf
        o1[self.Y[uid,],np.arange(T)] -= 1.0

        # the thing we pass through
        delta_t = np.zeros((self.K,))

        for tt in np.arange(T-1,-1,-1):
            # gradient on V
            dV += np.outer(o1[:,tt],np.hstack((g[:,tt],w[:,tt])))
            delta_gwt = np.dot(self.V.T,o1[:,tt])
            # now further on this delta_gwt induced by e
            delta_gwt += de[tt] * e[tt] * (1 - e[tt]) * (self.V[0,] - self.V[1,])
            dV[0,] += de[tt] * e[tt] * (1 - e[tt]) * np.hstack((g[:,tt],w[:,tt]))
            dV[1,] -= de[tt] * e[tt] * (1 - e[tt]) * np.hstack((g[:,tt],w[:,tt]))
            delta_t -= delta_gwt[self.K:2 * self.K]
            dp[:,tt] += delta_gwt[0:self.K]
            dg[:,uid] += delta_gwt[self.K:2 * self.K]
            for t in np.arange(tt-1,-1,-1):
                dR[t,tt] -= np.inner(delta_gwt[0:self.K],hl[:,t])
                dU -= self.R[t,tt] * e[t] * np.outer(delta_gwt[0:self.K],self.X[:,t])
                de[t] -= self.R[t,tt] * np.inner(delta_gwt[0:self.K],np.dot(self.U,self.X[:,t]))

            # now the part that gets propagate back
            if tt > 0:
                de[tt-1] += np.inner(delta_t,np.dot(self.U,self.X[:,tt-1]))
                # and dU/dW too!
                dU += e[tt-1] * np.outer(delta_t,self.X[:,tt-1])
                dW += np.outer(delta_t,h[:,tt-1])
            delta_t = np.dot(self.W.T,delta_t)

        return (dU,dV,dW,dp,dg,dR)

    def gd_one_step(self,uid):

        (dU,dV,dW,dp,dg,dR) = self.backprop(uid)




        # gradient check!!!
        # to make sure backprop is implemented correctly
        dU_check = np.zeros((self.K,self.D))
        d = .0001
        for ii in range(self.K):
            for jj in range(self.D):
                self.U[ii,jj] -= d
                cost_minus = self.loss_1pt(uid)
                self.U[ii,jj] += 2 * d
                cost_plus = self.loss_1pt(uid)
                dU_check[ii,jj] = (cost_plus - cost_minus) / (2.0 * d)
                self.U[ii,jj] -= d

        print('U grad',np.max(np.abs(dU - dU_check)))

        dV_check = np.zeros((self.A,2*self.K))
        d = .0001
        for ii in range(self.A):
            for jj in range(2*self.K):
                self.V[ii,jj] -= d
                cost_minus = self.loss_1pt(uid)
                self.V[ii,jj] += 2 * d
                cost_plus = self.loss_1pt(uid)
                dV_check[ii,jj] = (cost_plus - cost_minus) / (2.0 * d)
                self.V[ii,jj] -= d

        print('V grad',np.max(np.abs(dV - dV_check)))

        dW_check = np.zeros((self.K,self.K))
        d = .0001
        for ii in range(self.K):
            for jj in range(self.K):
                self.W[ii,jj] -= d
                cost_minus = self.loss_1pt(uid)
                self.W[ii,jj] += 2 * d
                cost_plus = self.loss_1pt(uid)
                dW_check[ii,jj] = (cost_plus - cost_minus) / (2.0 * d)
                self.W[ii,jj] -= d

        print('W grad',np.max(np.abs(dW - dW_check)))

        dp_check = np.zeros((self.K,self.S))
        d = .0001
        for ii in range(self.K):
            for jj in range(self.S):
                self.p[ii,jj] -= d
                cost_minus = self.loss_1pt(uid)
                self.p[ii,jj] += 2 * d
                cost_plus = self.loss_1pt(uid)
                dp_check[ii,jj] = (cost_plus - cost_minus) / (2.0 * d)
                self.p[ii,jj] -= d

        print('p grad',np.max(np.abs(dp - dp_check)))

        dg_check = np.zeros((self.K,self.N))
        d = .0001
        for ii in range(self.K):
            for jj in range(self.N):
                self.g[ii,jj] -= d
                cost_minus = self.loss_1pt(uid)
                self.g[ii,jj] += 2 * d
                cost_plus = self.loss_1pt(uid)
                dg_check[ii,jj] = (cost_plus - cost_minus) / (2.0 * d)
                self.g[ii,jj] -= d

        print('g grad', np.max(np.abs(dg - dg_check)))

        dR_check = np.zeros((self.S,self.S))
        d = .0001
        for ii in range(self.S):
            for jj in range(self.S):
                self.R[ii,jj] -= d
                cost_minus = self.loss_1pt(uid)
                self.R[ii,jj] += 2 * d
                cost_plus = self.loss_1pt(uid)
                dR_check[ii,jj] = (cost_plus - cost_minus) / (2.0 * d)
                self.R[ii,jj] -= d

        print('R grad', np.max(np.abs(dR - dR_check)))





        self.U -= self.alpha * dU
        self.V -= self.alpha * dV
        self.W -= self.alpha * dW
        self.p -= self.alpha * dp
        self.g -= self.alpha * dg
        self.R -= self.alpha * dR

    def train(self):
        # the training algorithm
        cr = np.zeros((self.epoch,))
        for it in range(self.epoch):
            # do gradient descent
            for uid in range(self.N):
                self.gd_one_step(uid)
                #print ii,np.max(self.W),np.max(self.U),np.max(self.V)
            # calculate loss
            cost = 0.0
            for uid in range(self.N):
                cost += self.loss_1pt(uid)
            # record and adjust learning rate and print stuff
            cr[it] = cost
            print(it,cost)
            if it > self.epoch / 10:
                if cr[it] > cr[it-1]:
                    self.alpha /= 2.0

np.random.seed(0)
# generate some fake data for gradient checking purposes
N = 100
S = 10
D = 5
Xtrain = np.random.normal(size=(D,S))
Ytrain = np.random.randint(0,4,size=(N,S))

np.random.seed(0)
q1 = RNN(Xtrain,Ytrain,alpha=0.01,hidden_dim=5,no_epoch=20)
q1.train()

print('astros!')