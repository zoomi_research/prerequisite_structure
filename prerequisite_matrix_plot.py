import pickle
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib
import io


with open('r_matrix_no_dropout.pickle', 'rb') as handle:
    r = pickle.load(handle)

def create_heatmap(matrix, num_topics, horizontal=None):
    xticks = ['Topic ' + str(i+1) for i in range(0,num_topics)]
    ax = sns.heatmap(matrix,yticklabels=5, xticklabels=xticks,cmap="Blues")
    try:
        for i in horizontal:
            plt.axhline(y=i, xmin=0, xmax=1, linewidth=1, color='k')
    except Exception as e:
        raise ValueError('Incorrect input for horizontal lines. Check that indices are in bounds and integers are input in the array.')

    ax.invert_yaxis()
    img_data = io.BytesIO()
    return img_data

def create_heatmap(matrix):
    # clean up the upper healf
    nrow, ncol = matrix.shape
    for iter_row in range(nrow):
        for iter_col in range(ncol):
            # if matrix[iter_row,iter_col]<0.05: matrix[iter_row,iter_col] = 0
            if iter_row>=iter_col: matrix[iter_row,iter_col] = 0
    ax = sns.heatmap(matrix,cmap= "Blues",yticklabels=5, xticklabels = 5)

    plt.axvline(x=7, color='black')
    plt.axvline(x=13, color='black')
    plt.axvline(x=21, color='black')
    return matrix